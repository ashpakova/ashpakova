package com.training.shpakova.task2;


import org.junit.Assert;
import org.junit.Test;


public class TenderTest {
    public static final Tender TENDER = new Tender();
    public static final Brigade [] BRIGADES = Service.findApplicants();
    public static final Brigade FOUND_WINNER = BRIGADES[1];
    public static final Brigade NOT_FOUND_WINNER = null;
    Brigade winner;
    int bricklayerAmount;
    int plastererAmount;
    int painterAmount;

    @Test
    public void testFindAdequateBrigadePositiveCase() {
        bricklayerAmount = 3;
        plastererAmount = 2;
        painterAmount = 1;
        winner = TENDER.findAdequateBrigade(BRIGADES, bricklayerAmount, plastererAmount, painterAmount);
        Assert.assertEquals(FOUND_WINNER, winner);
    }

    @Test
    public void testFindAdequateBrigadeNegativeCase() {
        bricklayerAmount = 3;
        plastererAmount = 3;
        painterAmount = 3;
        winner = TENDER.findAdequateBrigade(BRIGADES, bricklayerAmount, plastererAmount, painterAmount);
        Assert.assertEquals(NOT_FOUND_WINNER, winner);
    }

}